package me.tobi.ardacraft.spezialitems.listener;

import me.tobi.ardacraft.api.classes.Rasse;
import me.tobi.ardacraft.spezialitems.ItemManager;
import me.tobi.ardacraft.spezialitems.SpezialItem;
import me.tobi.ardacraft.spezialitems.SpezialItem.AttackMode;

import me.tobi.ardacraft.spezialitems.item.Menschenschild;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.flags.DefaultFlag;

public class ExecuteListener implements Listener {
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {			
		Player p = event.getPlayer();
		ItemStack item = p.getInventory().getItemInMainHand();
        ItemStack offHandItem = p.getInventory().getItemInOffHand();
		//No longer needed
		/*if(item.hasItemMeta()) {
			if(item.getItemMeta().hasLore()) {
				if(!item.getItemMeta().getLore().get(1).contains("Tier") && item.getItemMeta().getLore().get(0).contains("Spezialitem")) {
					Utils.removeOne(item, p);
					p.getInventory().addItem(ItemManager.getNew(item).getItemStack());
					p.updateInventory();
				}
			}
		}*/

		//The result outputs if the event can be passed on
		boolean result = true;

		if(item != null && ItemManager.isSpezialItem(item)){

            if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                result = handleRightClick(item, p);
            } else if (event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK) {
                result = handleLeftClick(item, p);
            }
        }

        if(offHandItem != null && ItemManager.isSpezialItem(offHandItem)){
            if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                result = handleRightClick(offHandItem, p);
            } else if (event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK) {
                result = handleLeftClick(offHandItem, p);
            }
        }

        if(!result){
            event.setCancelled(true);
        }
	}
	private boolean handleRightClick(ItemStack item, Player p){
        SpezialItem sitem = ItemManager.getSpezialItem(item);

        if(!sitem.getRassen().contains(Rasse.get(p)) && !sitem.getRassen().contains(Rasse.ALLE_RASSEN)) {
            return true;
        }

        if(item.getDurability() > 0) {
            item.setDurability((short)0);
        }

        sitem.onRightClick(p);

        return item.getType() != Material.BOW;

    }

    private boolean handleLeftClick(ItemStack item, Player p){
        SpezialItem sitem = ItemManager.getSpezialItem(item);

        if(!sitem.getRassen().contains(Rasse.get(p)) && !sitem.getRassen().contains(Rasse.ALLE_RASSEN)) {
            return true;
        }

        if(item.getDurability() > 0) {
            item.setDurability((short)0);
        }

        sitem.onLeftClick(p);

        return true;
    }

	@EventHandler (priority = EventPriority.HIGHEST)
	public void onProjectileLaunch(ProjectileLaunchEvent event) {
        if(event.getEntity().getShooter() instanceof Player) {
			Player p = (Player)event.getEntity().getShooter();
			ItemStack item = p.getInventory().getItemInMainHand();

            if(item != null) {
				if(ItemManager.isSpezialItem(item)) {
                    SpezialItem sitem = ItemManager.getSpezialItem(item);
					if(!sitem.getRassen().contains(Rasse.get(p)) && !sitem.getRassen().contains(Rasse.ALLE_RASSEN)) {
						return;
					}
                    sitem.onProjectileLaunch(p, event.getEntity());
				}
			}
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onProjectileHit(ProjectileHitEvent event) {
		if(event.getEntity().getShooter() instanceof Player) {
			Player p = (Player)event.getEntity().getShooter();
			ItemStack item = p.getInventory().getItemInMainHand();
			if(item != null) {
				if(ItemManager.isSpezialItem(item)) {
					SpezialItem sitem = ItemManager.getSpezialItem(item);
					if(!sitem.getRassen().contains(Rasse.get(p)) && !sitem.getRassen().contains(Rasse.ALLE_RASSEN)) {
						return;
					}
					sitem.onProjectileHit(p, event.getEntity());
				}
			}
		}
	}
	
	private WorldGuardPlugin getWorldGuard() {
	    Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("WorldGuard");
	 
	    // WorldGuard may not be loaded
	    if (plugin == null || !(plugin instanceof WorldGuardPlugin)) {
	        return null; // Maybe you want throw an exception instead
	    }
	 
	    return (WorldGuardPlugin) plugin;
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		/*if(event.getDamager() instanceof Player) {
			Player damager = (Player)event.getDamager();
			System.out.println("==================Damage report==================");
			System.out.println("Base: " + event.getDamage(DamageModifier.BASE));
			double strength = -1.0;
			if(damager.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
				strength = 1.5 * (PotionEffectManager.getAmplifier(damager, PotionEffectType.INCREASE_DAMAGE) + 1);
			}
			System.out.println("Staerke: " + strength);
			System.out.println("Armor: " + event.getDamage(DamageModifier.ARMOR));
			System.out.println("Blocking: " + event.getDamage(DamageModifier.BLOCKING));
			System.out.println("Resistenz: " + event.getDamage(DamageModifier.RESISTANCE));
			System.out.println("Absorption: " + event.getDamage(DamageModifier.ABSORPTION));
			System.out.println("Magic: " + event.getDamage(DamageModifier.MAGIC));
			System.out.println("Hard Hat: " + event.getDamage(DamageModifier.HARD_HAT));
			
			Utils.getDamageFor(damager, event.getEntity(), 20, false);
			
			System.out.println("==================================================");
		}*/
		if(event.getEntity() instanceof Fireball) {
			Fireball fb = (Fireball)event.getEntity();
			if(fb.getShooter() instanceof Player) {
				event.setCancelled(true);
			}
		}
		if(event.getDamager() instanceof Player) {

            @SuppressWarnings("deprecation")
            boolean pvp = getWorldGuard().getRegionManager(event.getEntity().getWorld()).
                    getApplicableRegions(event.getEntity().getLocation()).allows(DefaultFlag.PVP);
            if(pvp == false) {
                return;
            }

			Player attacker = (Player) event.getDamager();
			if(attacker.getInventory().getItemInMainHand() != null
                    && ItemManager.isSpezialItem(attacker.getInventory().getItemInMainHand())
                    && (!(ItemManager.getSpezialItem(attacker.getInventory().getItemInMainHand()) instanceof Menschenschild)) ) {

				SpezialItem sitem = ItemManager.getSpezialItem(attacker.getInventory().getItemInMainHand());

				if(!sitem.getRassen().contains(Rasse.get(attacker)) && !sitem.getRassen().contains(Rasse.ALLE_RASSEN)) {
					return;
				}
				if(sitem.getAttackMode() == AttackMode.ATTACK) {
					sitem.onHit(attacker, event.getEntity());
					event.setCancelled(true);
				}
			}

            if(attacker.getInventory().getItemInOffHand() != null
                    && ItemManager.isSpezialItem(attacker.getInventory().getItemInOffHand())
                    && (ItemManager.getSpezialItem(attacker.getInventory().getItemInMainHand()) instanceof Menschenschild)) {

                SpezialItem sitem = ItemManager.getSpezialItem(attacker.getInventory().getItemInMainHand());

                if(!sitem.getRassen().contains(Rasse.get(attacker)) && !sitem.getRassen().contains(Rasse.ALLE_RASSEN)) {
                    return;
                }
                if(sitem.getAttackMode() == AttackMode.ATTACK) {
                    sitem.onHit(attacker, event.getEntity());
                    event.setCancelled(true);
                }
            }

            Player hitPlayer = (Player) event.getDamager();
            if(hitPlayer.getInventory().getItemInMainHand() != null
                    && ItemManager.isSpezialItem(hitPlayer.getInventory().getItemInOffHand())) {

                ItemManager.getSpezialItem(hitPlayer.getInventory().getItemInOffHand()).onPlayerGetsHit(event, event.getDamager(), hitPlayer);

            }
        }

		if(event.getDamager() instanceof Projectile) {
			Projectile ar = (Projectile)event.getDamager();
			if(ar.getShooter() instanceof Player) {
				Player attacker = (Player) ar.getShooter();
				if(attacker.getInventory().getItemInMainHand() != null) {
					if(ItemManager.isSpezialItem(attacker.getInventory().getItemInMainHand())) {
						SpezialItem sitem = ItemManager.getSpezialItem(attacker.getInventory().getItemInMainHand());
						@SuppressWarnings("deprecation")
						boolean pvp = getWorldGuard().getRegionManager(event.getEntity().getWorld())
								.getApplicableRegions(event.getEntity().getLocation()).allows(DefaultFlag.PVP);
						if (pvp == false) {
							return;
						}
						if(!sitem.getRassen().contains(Rasse.get(attacker)) && !sitem.getRassen().contains(Rasse.ALLE_RASSEN)) {
							return;
						}
						if(sitem.getAttackMode() == AttackMode.PROJECTILE) {
							sitem.onHit(attacker, event.getEntity()); //TODO unschön
						}
					}
				}
			}
		}
	}
	
	/*public static HashMap<Player, List<ItemStack>> spezItemsDeath = new HashMap<Player, List<ItemStack>>();
	
	@EventHandler
	public void onEntityDeath(EntityDeathEvent event) { //TODO fix double death
		if(event.getEntity() instanceof Player) {
			List<ItemStack> spezitems = new ArrayList<ItemStack>();
			Iterator<ItemStack> itor = event.getDrops().iterator();
			while(itor.hasNext()) {
				ItemStack current = itor.next();
				if(ItemManager.isSpezialItem(current)) {
					spezitems.add(current);
					itor.remove();
				}
			}
			spezItemsDeath.put((Player)event.getEntity(), spezitems);
			String spzitems = "";
			for(ItemStack item : spezitems) {
				spzitems += "[" + item.getItemMeta().getLore().get(2) + ", " + item.getItemMeta().getLore().get(1) + "]";
			}
			System.out.println("keeping for " + event.getEntity().getName() + ": " + spzitems);
		}
	}
	
	@EventHandler
	public void onPlayerRespawn(PlayerRespawnEvent event) {
		Player p = event.getPlayer();
		if(spezItemsDeath.containsKey(p)) {
			for(ItemStack si : spezItemsDeath.get(p)) {
				p.getInventory().addItem(si);
			}
			spezItemsDeath.remove(p);
		}
	}*/
	
}
