package me.tobi.ardacraft.spezialitems.listener;

import me.tobi.ardacraft.api.classes.Rasse;
import me.tobi.ardacraft.api.classes.Utils;
import me.tobi.ardacraft.spezialitems.ItemManager;
import me.tobi.ardacraft.spezialitems.SpezialItem;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class UpgradeListener implements Listener{
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onPlayerInteract(PlayerInteractEvent event) {
		if(event.getAction() == Action.LEFT_CLICK_BLOCK && event.getPlayer().isSneaking()) {
			if(event.getClickedBlock().getType() == Material.ANVIL) {
				Player p = event.getPlayer();
				ItemStack item = p.getInventory().getItemInMainHand();
				if(item != null) {
					if(ItemManager.isSpezialItem(item)) {
						SpezialItem sitem = ItemManager.getSpezialItem(item);
						if(sitem.getTier() == 3) {
							p.sendMessage("§cDu kannst dein Spezialitem nicht mehr aufrüsten!");
							return;
						}
						if(!sitem.getRassen().contains(Rasse.get(p)) && !sitem.getRassen().contains(Rasse.ALLE_RASSEN)) {
							return;
						}
						if(ItemManager.getNextTier(item) != null) {
							if(p.isSneaking()) {
								if(item.getAmount() > 1) {
									p.sendMessage("§cDu darfst zum Upgraden nur ein Item in der Hand halten");
									return;
								}
								if(sitem.getTier() == 0) {
									if(p.getInventory().contains(Material.EMERALD)) {
										Utils.removeOne(Material.EMERALD, p);
										item = sitem.upgrade(item);
										p.sendMessage("§aDu hast dein Spezialitem erfolgreich auf Tier 1 aufgerüstet!");
									}
								}else if(sitem.getTier() == 1) {
									if(p.getInventory().contains(Material.DIAMOND_BLOCK)) {
										Utils.removeOne(Material.DIAMOND_BLOCK, p);
										item = sitem.upgrade(item);
										p.sendMessage("§aDu hast dein Spezialitem erfolgreich auf Tier 2 aufgerüstet!");
									}
								}else if(sitem.getTier() == 2) {
									if(p.getInventory().contains(Material.NETHER_STAR)) {
										Utils.removeOne(Material.NETHER_STAR, p);
										item = sitem.upgrade(item);
										p.sendMessage("§aDu hast dein Spezialitem erfolgreich auf Tier 3 aufgerüstet!");
									}
								}
							}else {
								p.sendMessage("§cDu musst Sneaken um dein Spezialitem aufzurüsten!");
							}
						}
					}else { //no tier
						for(SpezialItem si : ItemManager.getRegistered()) {
							if(si.getRassen().contains(Rasse.get(p)) || si.getRassen().contains(Rasse.ALLE_RASSEN)) {
								if(item.getType() == si.getMaterial()) {
									if(p.isSneaking()) {
										if(item.getAmount() == 1) {
											if(p.getInventory().contains(Material.DIAMOND)) {
												Utils.removeOne(item, p);
												Utils.removeOne(Material.DIAMOND, p);
												si.setTier(0);
												p.getInventory().addItem(si.getItemStack());
												p.updateInventory();
												p.sendMessage("§aDu hast eine(n) " + si.getName() + "§r§a Tier 0 erhalten!");
											}else {
												p.sendMessage("§cFür ein SpezialItem-Upgrade brauchst du einen Diamanten!");
											}
										}else {
											p.sendMessage("§cDu darfst zum Upgraden nur ein Item in der Hand halten");
										}
									}else {
										p.sendMessage("§cFür ein Spezial-Item-Upgrade musst du Sneaken!");										
									}
								}
							}
						}	
					}
				}
			}
		}
	}
	
}
