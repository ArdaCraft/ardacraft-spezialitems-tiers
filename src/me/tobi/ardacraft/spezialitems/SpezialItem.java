package me.tobi.ardacraft.spezialitems;

import java.util.ArrayList;
import java.util.List;

import me.tobi.ardacraft.api.classes.Rasse;

import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public abstract class SpezialItem {
	
	public SpezialItem(Integer tier) {
		this.tier = tier;
	}
	
	protected Material material;
	protected String name;
	protected byte data;
	protected Integer damage;
	protected AttackMode attackMode;
	protected int tier;
	protected List<Rasse> rassen = new ArrayList<Rasse>();
	
	@SuppressWarnings("deprecation")
	public ItemStack getItemStack() {
		ItemStack rtn = new ItemStack(material, 1, (short)0, data);
		ItemMeta meta = rtn.getItemMeta();
		meta.setDisplayName("§c§l" + name);
		List<String> lore = new ArrayList<String>();
		lore.add("Spezialitem");
		lore.add("Tier " + tier);
        lore.add(name);
        lore.add("Quest Item");
        meta.setLore(lore);
		rtn.setItemMeta(meta);
		return rtn;		
	}
	
	public ItemStack upgrade(ItemStack i) {
		if(tier == 3) {
			return null;
		}
		tier++;
		List<String> lore = new ArrayList<String>();
		lore.add("Spezialitem");
		lore.add("Tier " + tier);
		lore.add(name);
        lore.add("Quest Item");
		ItemMeta meta = i.getItemMeta();
		meta.setLore(lore);
		i.setItemMeta(meta);
		return i;
	}
	
	public SpezialItem getTier(int tier) {
		this.tier = tier;
		return this;
	}
	
	public void onRightClick(Player p) {}
	public void onLeftClick(Player p) {}
	public void onProjectileLaunch(Player p, Projectile pr) {}
	public void onProjectileHit(Player p, Projectile pr) {}
	public void onHit(Player attacker, Entity hit) {}
	public void onPlayerGetsHit(EntityDamageByEntityEvent event, Entity attacker, Player hitPlayer) {}

	public List<Rasse> getRassen() {
		return rassen;
	}
	
	public void setTier(int tier) {
		this.tier = tier;
	}
	
	public int getDamage() {
		return damage;
	}
	
	public String getName() {
		return name;
	}

	public int getTier() {
		return tier;
	}
	
	public Material getMaterial() {
		return material;
	}
	
	public AttackMode getAttackMode() {
		return attackMode;
	}

	public enum AttackMode {
		ATTACK, PROJECTILE, NONE;
	}

	
}
