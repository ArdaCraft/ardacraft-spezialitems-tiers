package me.tobi.ardacraft.spezialitems;

import java.util.ArrayList;
import java.util.List;

import me.tobi.ardacraft.spezialitems.item.*;
import me.tobi.ardacraft.spezialitems.item.Menschenschild;

import org.bukkit.inventory.ItemStack;

public class ItemManager {
	
	private static List<SpezialItem> items = new ArrayList<SpezialItem>();
	
	public static void registerItem(SpezialItem si) {
		items.add(si);
	}
	
	public static List<SpezialItem> getRegistered() {
		return items;
	}
	
	public static boolean isSpezialItem(ItemStack item) {
		if(item.hasItemMeta()) {
			if(item.getItemMeta().hasLore()) {
				if(item.getItemMeta().getLore().get(0).equalsIgnoreCase("Spezialitem")) {
					if(item.getItemMeta().getLore().get(1).contains("Tier")) {
						return true;
					}
				}
			}else {
				return false;
			}
		}else {
			return false;
		}
		return false;
	}
	
	public static SpezialItem getNew(ItemStack i) {
		if(i.getItemMeta().getLore().get(1).contains("Armbrust")) {
			return new Armbrust(0);
		}else if(i.getItemMeta().getLore().get(1).contains("Elbenbogen")){
			return new Elbenbogen(0);
		}else if(i.getItemMeta().getLore().get(1).contains("Magierstab")){
			return new Magierstab(0);
		}else if(i.getItemMeta().getLore().get(1).contains("Dolch")){
			return new Dolch(0);
		}else if(i.getItemMeta().getLore().get(1).contains("Kampfaxt")){
			return new Kampfaxt(0);
		}else if(i.getItemMeta().getLore().get(1).contains("Krummschwert")){
			return new Keule(0);
		}else if(i.getItemMeta().getLore().get(1).contains("Menschenschild")){
			return new Menschenschild(0);
		}else if(i.getItemMeta().getLore().get(1).contains("Ring")){
			return new Ring(0);
		}else {
			return null;
		}
	}
	
	public static SpezialItem getSpezialItem(ItemStack item) {
		if(isSpezialItem(item)) {
			
			String tierstr = item.getItemMeta().getLore().get(1);
			tierstr = tierstr.substring(5);
			int tier = Integer.valueOf(tierstr);
			
			if(item.getItemMeta().getLore().get(2).contains("Armbrust")) {
				return new Armbrust(tier);
			}else if(item.getItemMeta().getLore().get(2).contains("Elbenbogen")){
				return new Elbenbogen(tier);
			}else if(item.getItemMeta().getLore().get(2).contains("Magierstab")){
				return new Magierstab(tier);
			}else if(item.getItemMeta().getLore().get(2).contains("Dolch")){
				return new Dolch(tier);
			}else if(item.getItemMeta().getLore().get(2).contains("Kampfaxt")){
				return new Kampfaxt(tier);
			}else if(item.getItemMeta().getLore().get(2).contains("Keule")){
				return new Keule(tier);
			}else if(item.getItemMeta().getLore().get(2).contains("Menschenschild")){
				return new Menschenschild(tier);
			}else if(item.getItemMeta().getLore().get(2).contains("Der Ring")){
				return new Ring(tier);
			}else {
				return null;
			}
		}else {
			throw new ClassCastException("Cannot convert a normal " + item.getType().toString() + " into a SpezialItem!");
		}
	}
	
	public static SpezialItem getNextTier(ItemStack item) {
		if(!isSpezialItem(item)) {
			return null;
		}else {
			if(getSpezialItem(item).getTier() == 3) {
				return null;
			}
			for(SpezialItem si : items) {
				if(item.getItemMeta().getLore().get(2).equalsIgnoreCase(si.getName())) {
					return si.getTier(si.getTier() + 1);
				}
			}
			return null;
		}
	}
	
}
