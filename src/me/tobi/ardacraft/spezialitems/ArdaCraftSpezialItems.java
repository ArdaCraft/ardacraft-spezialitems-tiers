package me.tobi.ardacraft.spezialitems;

import me.tobi.ardacraft.spezialitems.item.Armbrust;
import me.tobi.ardacraft.spezialitems.item.Dolch;
import me.tobi.ardacraft.spezialitems.item.Elbenbogen;
import me.tobi.ardacraft.spezialitems.item.Kampfaxt;
import me.tobi.ardacraft.spezialitems.item.Keule;
import me.tobi.ardacraft.spezialitems.item.Magierstab;
import me.tobi.ardacraft.spezialitems.item.Menschenschild;
import me.tobi.ardacraft.spezialitems.item.Ring;
import me.tobi.ardacraft.spezialitems.listener.ExecuteListener;
import me.tobi.ardacraft.spezialitems.listener.UpgradeListener;
import me.tobi.ardacraft.spezialitems.scheduler.EffectScheduler;

import org.bukkit.plugin.java.JavaPlugin;

public class ArdaCraftSpezialItems extends JavaPlugin{
	
	static ArdaCraftSpezialItems plugin;
	
	@Override
	public void onEnable() {
		getServer().getPluginManager().registerEvents(new UpgradeListener(), this);
		getServer().getPluginManager().registerEvents(new ExecuteListener(), this);
		
		plugin = this;
		EffectScheduler ef = new EffectScheduler();
		ef.run();
		
		ItemManager.registerItem(new Armbrust(0));
		ItemManager.registerItem(new Elbenbogen(0));
		ItemManager.registerItem(new Magierstab(0));
		ItemManager.registerItem(new Dolch(0));
		ItemManager.registerItem(new Kampfaxt(0));
		ItemManager.registerItem(new Keule(0));
		ItemManager.registerItem(new Menschenschild(0));
		ItemManager.registerItem(new Ring(0));
	}
	
	public static ArdaCraftSpezialItems getPlugin() {
		return plugin;
	}
	
}
