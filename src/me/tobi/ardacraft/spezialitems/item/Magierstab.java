package me.tobi.ardacraft.spezialitems.item;

import me.tobi.ardacraft.api.classes.Rasse;
import me.tobi.ardacraft.api.classes.Utils;
import me.tobi.ardacraft.spezialabilities.SpezialAbility;
import me.tobi.ardacraft.spezialitems.SpezialItem;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.*;

public class Magierstab extends SpezialItem{

	public Magierstab(Integer tier) {
		super(tier);
		super.attackMode = AttackMode.PROJECTILE;
		super.material = Material.STICK;
		super.name = "Magierstab";
		super.rassen.add(Rasse.MAGIER);
	}
	
	static int fballs = 0;
	
	@Override
	public void onRightClick(final Player p) {
		if(tier == 0) {
			SpezialAbility.LAUNCH_WITHERSKULL.execute(p, null, 60);
		}else if(tier == 1) {
			SpezialAbility.LAUNCH_WITHERSKULL.execute(p, null, 20);			
		}else if(tier == 2) {
			SpezialAbility.LAUNCH_FIREBALL.execute(p, null, 15);
		}else if(tier == 3) {
			SpezialAbility.LAUNCH_FIREBALL.execute(p, null, 5);
		}
	}
	
	@Override
	public void onLeftClick(Player p) {
		if(tier == 3) {
			SpezialAbility.STRIKE_LIGHTNINGS_DELAYED.execute(p, null, 30);
		}
	}
	
	@Override
	public void onHit(Player attacker, Entity hit) {
		if(hit instanceof LivingEntity) {	
			if(tier == 0) {
				((LivingEntity) hit).damage(Utils.getDamageFor(attacker, hit, 10, true));
			}else if(tier == 1) {
				((LivingEntity) hit).damage(Utils.getDamageFor(attacker, hit, 14, true));
			}else if(tier == 2) {
				((LivingEntity) hit).damage(Utils.getDamageFor(attacker, hit, 20, true));
			}else if(tier == 3) {
				((LivingEntity) hit).damage(Utils.getDamageFor(attacker, hit, 20, true));
			}
		}
	}

	@Override
    public void onProjectileHit(Player shooter, Projectile pr){
	    if(pr instanceof Fireball){
	        if(tier == 3 && shooter.getWorld().getName().equalsIgnoreCase("RPG")){
	            Bukkit.getWorld("RPG").strikeLightning(pr.getLocation());
            }
        }
    }

}
