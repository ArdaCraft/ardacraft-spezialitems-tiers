package me.tobi.ardacraft.spezialitems.item;

import java.util.Random;

import me.tobi.ardacraft.api.classes.Rasse;
import me.tobi.ardacraft.api.classes.Utils;
import me.tobi.ardacraft.spezialitems.SpezialItem;

import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Elbenbogen extends SpezialItem{

	public Elbenbogen(Integer tier) {
		super(tier);
		super.attackMode = AttackMode.PROJECTILE;
		super.material = Material.BOW;
		super.name = "Elbenbogen";
		super.rassen.add(Rasse.ELB);
		super.rassen.add(Rasse.WARGREITER);
		
	}
	
	@Override
	public void onHit(Player attacker, Entity hit) { //TODO include
		if(hit instanceof LivingEntity) {
			if(tier == 0) {
				((LivingEntity) hit).damage(Utils.getDamageFor(attacker, hit, 9, true));
				attacker.getInventory().addItem(new ItemStack(Material.ARROW, 1));
			}else if(tier == 1) {
				((LivingEntity) hit).damage(Utils.getDamageFor(attacker, hit, 12, true));
				Random rnd = new Random();
				attacker.getInventory().addItem(new ItemStack(Material.ARROW, rnd.nextInt(2) + 1));
			}else if(tier == 2) {
				((LivingEntity) hit).damage(Utils.getDamageFor(attacker, hit, 22, true));
				Random rnd = new Random();
				attacker.getInventory().addItem(new ItemStack(Material.ARROW, rnd.nextInt(2) + 1));
			}else if(tier == 3) {
				((LivingEntity) hit).damage(Utils.getDamageFor(attacker, hit, 32, true));
			}
		}
	}
	
	@Override
	public void onProjectileLaunch(Player p, Projectile pr) {
		if(tier == 2) {
			pr.setFireTicks(Integer.MAX_VALUE);
		}else if(tier == 3) {
			pr.setFireTicks(Integer.MAX_VALUE);
			p.getInventory().addItem(new ItemStack(Material.ARROW, 1));
		}
	}
	
	@Override
	public void onProjectileHit(Player p, Projectile pr) {
		pr.remove();
		if(tier == 3) {
			//p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 5, 9));
			p.getWorld().createExplosion(pr.getLocation(), 1.5F);
			pr.remove();
		}
	}
	
}
