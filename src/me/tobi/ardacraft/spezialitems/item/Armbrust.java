package me.tobi.ardacraft.spezialitems.item;

import me.tobi.ardacraft.api.classes.Rasse;
import me.tobi.ardacraft.api.classes.Utils;
import me.tobi.ardacraft.spezialabilities.SpezialAbility;
import me.tobi.ardacraft.spezialitems.ArdaCraftSpezialItems;
import me.tobi.ardacraft.spezialitems.SpezialItem;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.inventory.ItemStack;

public class Armbrust extends SpezialItem{
	
	public Armbrust(Integer tier) {
		super(tier);
		super.material = Material.STICK;
		super.attackMode = AttackMode.PROJECTILE;
		super.name = "Armbrust";
		super.rassen.add(Rasse.URUKHAI);
		super.rassen.add(Rasse.DUNEDAIN);
	}
	
	@Override
	public void onRightClick(Player p) {
		if(tier == 0) {
			if(p.getInventory().contains(Material.ARROW)) {
				int state = SpezialAbility.LAUNCH_ARROW.execute(p, true, 60);
				if(state == 0) {
					Utils.removeOne(Material.ARROW, p);
				}
			}
		}else if(tier == 1) {
			if(p.getInventory().contains(Material.ARROW)) {
				int state = SpezialAbility.LAUNCH_ARROW.execute(p, true, 30);
				if(state == 0) {
					Utils.removeOne(Material.ARROW, p);
				}
			}
		}else if(tier == 2) {
			if(p.getInventory().contains(Material.ARROW)) {
				int state = SpezialAbility.LAUNCH_ARROW.execute(p, true, 20);

				if(state == 0) {
					Utils.removeOne(Material.ARROW, p);
				}
			}

		}else if(tier == 3) {
			if(p.getInventory().contains(Material.ARROW)) {
				int state = SpezialAbility.LAUNCH_ARROW.execute(p, false, 10);

				if(state == 0) {
					Utils.removeOne(Material.ARROW, p);
				}
			}
		}
	}
	
	@Override
	public void onLeftClick(Player p) {
		if(tier == 3) {
			SpezialAbility.LAUNCH_ARROW_BURNING.execute(p, false, 100);			
		}
	}
	
	@Override
	public void onProjectileHit(final Player p, final Projectile pr) {
		if(pr.getFireTicks() > 0) {
			Bukkit.getScheduler().scheduleSyncDelayedTask(ArdaCraftSpezialItems.getPlugin(), new Runnable() {
				
				@Override
				public void run() {
					p.getWorld().createExplosion(pr.getLocation(), 1.2F);
					pr.remove();
				}				
				
			}, 20);
		}
	}
	
	@Override
	public void onHit(Player attacker, Entity hit) { //TODO develop
		if(hit instanceof LivingEntity) {
			if(tier == 0) {
				((LivingEntity) hit).damage(Utils.getDamageFor(attacker, hit, 8, true));
			}else if(tier == 1) {
				((LivingEntity) hit).damage(Utils.getDamageFor(attacker, hit, 14, true));
			}else if(tier == 2) {
				((LivingEntity) hit).damage(Utils.getDamageFor(attacker, hit, 20, true));
				if(hit instanceof HumanEntity) {
					HumanEntity entity = (HumanEntity)hit;
					ItemStack chest = entity.getInventory().getChestplate();
					if(chest != null) {
						chest.setDurability((short) (chest.getDurability() + 20));
					}
				}
			}else if(tier == 3) {
				((LivingEntity) hit).damage(Utils.getDamageFor(attacker, hit, 28, true));
			}
		}
	}

}
